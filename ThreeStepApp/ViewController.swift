//
//  ViewController.swift
//  ThreeStepApp
//
//  Created by Uchenna Chilaka on 7/7/15.
//  Copyright (c) 2015 Comenity Servicing LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBackToHello() {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            // do some stuff
        });
    }

}

